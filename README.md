# Project 1 - Odin Recipes

In this project I will show the following fundamental basic HTML skills:

- Setting up the boilerplate
- Using heading tags
- Using lists
- Using HTML, headers, and body tags while inserting the right tags inside
  each of the respective tags
- Having each tag have the right attributes associated with them
- Have the right folder structure

# Reflections

Through the project I learnt the basic needs of the structure that the HTML on a website would make up and how to apply that to each page that I created.
It wasn't directly related to the project but what I did struggle with is getting it deployed to Gitlab Pages as it was very different in how GitLab does it which caused me some trouble until I found out that I can auto generate the file I need to online for it. I then thought that everything needs to be in the public folder for it to work but while that worked I later found out that I could of just renamed the public part of the yml folder to 'odin-recipes' and that should of work which I will take into account for next time.
